#!/bin/zsh

tippecanoe \
  --detect-shared-borders \
  --force \
  --maximum-zoom=10 \
  --no-tile-compression \
  --simplification=10 \
  --named-layer=micro-regions:<(cat public/micro-regions/*.geojson.json) \
  --named-layer=micro-regions_elevation:<(cat public/micro-regions_elevation/*.geojson.json) \
  --named-layer=outline:<(cat public/outline/*.geojson.json) \
  --output-to-directory=public/pbf/
