# EAWS Regions

Provides all region definitions for EAWS (and more).

https://regions.avalanches.org/

This repository provides the regions in a GeoJSON format and theire names in different languages.
In addition, the historical polygons are also included to process older bulletins. For this there are the attributes `valid_from` and `valid_until` in the polygons JSON property, which indicate in which period these were used.
Current polygons have no end of validity.

## Latest regions

The repository contains the current regions as well as a history and therefore "outdated" regions. A subset containing only the currently valid regions can be found within the downloadable artifact in the additional "latest" folders:
https://gitlab.com/eaws/eaws-regions/-/jobs/artifacts/master/download?job=pages