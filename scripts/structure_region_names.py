import os,glob
import pathlib
import json

def _finditem(obj, key):
    if key in obj: return obj[key]
    for k, v in obj.items():
        if isinstance(v,dict):
            item = _finditem(v, key)
            if item is not None:
                return item

print('start script')
pathlib.Path().resolve()
folder_path = 'public/micro-regions_names/'
for filename in glob.glob(os.path.join(folder_path, '*.json')):
  with open(filename, 'r') as f:
    print('work on', filename)
    json_unstructured = json.load(f)
    structured = {}
    structure = {} # Holds the calculated parent element for each region (by ID in the list l_unstruct) 
    l_unstruct = list(json_unstructured)
    l_unstruct_values = list(json_unstructured.values())
    id_i = len(json_unstructured)
    while id_i > 0:
        id_i = id_i - 1
        id_j = 0
        while id_j < len(json_unstructured):
            if l_unstruct[id_j] in l_unstruct[id_i] and l_unstruct[id_j] != l_unstruct[id_i]:
                structure[id_i] = id_j
            if not id_i in structure:
                structure[id_i] = id_i
            id_j = id_j + 1
    for i in range(len(structure)):
        if i == structure[i]:
            if i in structure.values():
                structured[l_unstruct[structure[i]]] = {
                    'RegionID': l_unstruct[i],
                    'Name': l_unstruct_values[i],
                    'Regions': {}
                }
            else:
                structured[l_unstruct[structure[i]]] = {
                    'RegionID': l_unstruct[i],
                    'Name': l_unstruct_values[i],
                }
        else:
            try:
                if i in structure.values():
                    _finditem(structured, l_unstruct[structure[i]])['Regions'][l_unstruct[i]] = {
                        'RegionID': l_unstruct[i],
                        'Name': l_unstruct_values[i],
                        'Regions': {}
                    }
                else:
                    _finditem(structured, l_unstruct[structure[i]])['Regions'][l_unstruct[i]] = {
                        'RegionID': l_unstruct[i],
                        'Name': l_unstruct_values[i],
                    }
                
            except:
                pass

    pathlib.Path(folder_path + 'structured').mkdir(parents=True, exist_ok=True)
    with open(folder_path + 'structured/' + filename.split('/')[-1], 'w') as outfile:
        print('export', folder_path + 'structured/' + filename.split('/')[-1])
        json.dump(structured, outfile, indent=4)
print('done!')
    
